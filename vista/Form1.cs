﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controlador;

namespace vista
{
    public partial class Form1 : Form
    {
        #region Objetos
        clsControlador objControlador = null;
        clsVariables objVariables = null;
        #endregion
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSumar_Click(object sender, EventArgs e)
        {
            mtSumaMVC();
        }
        private void mtSumaMVC() {
            objVariables = new clsVariables();
            objVariables.numero1 = Convert.ToInt32(txtNumero1.Text);

            clsControlador clsControlador = new clsControlador(objVariables);
            clsControlador.mtSumarValores();

            lblResultado.Text = objVariables.resultado.ToString();
        }

    }
}
