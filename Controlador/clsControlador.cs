﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;

namespace Controlador
{
    public class clsControlador
    {
        #region Variable
        clsVariables objVariables = null;
        #endregion

        public clsControlador(clsVariables objvar)
        {
            objVariables = objvar;
        }

        public void mtSumarValores()
        {

            clsModelo clsDatos = new clsModelo();
            objVariables.numero2 = clsDatos.mtSumar();
            if (objVariables.numero1 >= 10)
            {
                objVariables.resultado = objVariables.numero1 + objVariables.numero2;
            }
            
           
        }
    }
}
